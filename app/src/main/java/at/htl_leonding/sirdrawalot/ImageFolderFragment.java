package at.htl_leonding.sirdrawalot;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Base64;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created by rene on 23.07.2015.
 */
public class ImageFolderFragment extends Fragment {

    private static final int RESULT_IMAGE = 69;
    private static Intent mData;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, RESULT_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == 0) {
            onBackPressed();
        }
        switch (requestCode) {
            case RESULT_IMAGE :
                if (null != data) {
                    mData = data;
                }
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        final Fragment fragment = this;
        if (mData != null) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    TcpClient.SendImageTask sendImageTask = new TcpClient.SendImageTask(fragment);
                    sendImageTask.execute(mData);
                    mData = null;
                }
            }, 50);
        }
    }

    private void onBackPressed() {
        getFragmentManager().beginTransaction()
                .replace(R.id.main_container, new GalleryFragment()).commit();
    }
}
