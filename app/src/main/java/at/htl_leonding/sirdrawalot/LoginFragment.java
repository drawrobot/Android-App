package at.htl_leonding.sirdrawalot;

import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;


/**
 * Fragment which enables the user to login to the app
 */
public class LoginFragment extends Fragment implements View.OnClickListener, View.OnTouchListener {
    private static EditText mEtPassword;
    private static EditText mEtUsername;
    private static Button mBtnLogin;
    private static TextView mTvErrorMsg;
    private Button mBtnChanges;

    public static EditText getEtPassword() {
        return mEtPassword;
    }

    public static EditText getEtUsername() {
        return mEtUsername;
    }

    public static TextView getTvErrorMsg() {
        return mTvErrorMsg;
    }

    public LoginFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Login");

        View rootView = inflater.inflate(R.layout.fragment_login, container, false);
        mTvErrorMsg = (TextView) rootView.findViewById(R.id.tv_error_msg);
        mEtUsername = (EditText) rootView.findViewById(R.id.et_username);
        mEtPassword = (EditText) rootView.findViewById(R.id.et_password);
        mBtnLogin = (Button) rootView.findViewById(R.id.btn_login);
        mBtnChanges = (Button) rootView.findViewById(R.id.btn_changes);

        mEtUsername.setOnTouchListener(this);
        mEtPassword.setOnTouchListener(this);
        mBtnLogin.setOnClickListener(this);
        mBtnChanges.setOnClickListener(this);

        return rootView;
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_login) {
            final TcpClient.LoginTask loginTask = new TcpClient.LoginTask(getActivity());
            loginTask.execute(mEtUsername.getText().toString(), mEtPassword.getText().toString());
        }

        if (v.getId() == R.id.btn_changes) {
            changeHostAndPortDialog();
        }
    }
    private void changeHostAndPortDialog() {
        final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .title("Change Host / Port")
                .customView(R.layout.dialog_changes, true)
                .autoDismiss(true)
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        EditText etHost = (EditText) dialog.getCustomView().findViewById(R.id.et_host);
                        EditText etPort = (EditText) dialog.getCustomView().findViewById(R.id.et_port);

                        String newHost = etHost.getText().toString();
                        String newPort = etPort.getText().toString();

                        if (!newHost.equals(""))
                            TcpClient.IP = newHost;
                        if (!newPort.equals(""))
                            TcpClient.PORT = Integer.valueOf(newPort);

                        Toast.makeText(getActivity(), "Saved (Host: "
                                + TcpClient.IP + " Port: " + TcpClient.PORT + ")", Toast.LENGTH_LONG).show();
                        super.onPositive(dialog);
                    }
                })
                .build();

        dialog.setActionButton(DialogAction.POSITIVE, "OK");
        dialog.setActionButton(DialogAction.NEGATIVE, "CANCEL");
        dialog.show();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (v.getId() == R.id.et_username || v.getId() == R.id.et_password) {
            mEtUsername.getBackground().setColorFilter(null);
            mEtPassword.getBackground().setColorFilter(null);
            mTvErrorMsg.setText("");
        }
        return false;
    }
}
