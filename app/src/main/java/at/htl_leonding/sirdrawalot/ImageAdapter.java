package at.htl_leonding.sirdrawalot;

import java.util.ArrayList;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class ImageAdapter extends BaseAdapter {

    public static boolean cancelTasks = false;
    private Activity mContext;
    private static ArrayList<Item> mItems;


    public ImageAdapter(Activity context, String[] ids) {
        this.mContext = context;

        mItems = new ArrayList<>();
        for (String id : ids) {
            if (!id.equals("")) mItems.add(new Item(id));
        }

        new LoadImageTask().execute(0);
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    public void removeItem(int position) {
        Log.d("LoadImageTask", "Remove "+position);
        mItems.remove(position);
    }

    @Override
    public Item getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        ImageView imageView;
        if (convertView == null) {
            int dp10 = Utils.getDp(mContext, 10);
            imageView = new ImageView(mContext, null);
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            imageView.setPadding(dp10,dp10,dp10,dp10);
            imageView.setLayoutParams(new GridView.LayoutParams(
                    300, 300));

        } else {
            imageView = (ImageView) convertView;
        }

        imageView.setImageBitmap(mItems.get(position).bitmap);
        return imageView;
    }

    class LoadImageTask extends AsyncTask<Integer,Void,Integer> {

        @Override
        protected Integer doInBackground(Integer... ints) {
            Log.d("AsyncTask started", ""+ints[0]);
            if (mItems.size() != 0) {
                final Bitmap bitmap = TcpClient.getInstance().getSmallImageBitmap(mItems.get(ints[0]).id);
                mItems.get(ints[0]).bitmap = bitmap;
            }
            return ints[0];
        }

        @Override
        protected void onPostExecute(Integer id) {
            if (!cancelTasks) {
                id++;
                Log.d("LoadImageTask", ""+id);
                if (id < mItems.size()) {
                    new LoadImageTask().execute(id);
                }
            }
            GalleryFragment.invalidateGridViewViews();
        }
    }
}

class Item {
    public Bitmap bitmap;
    public String id;

    public Item(String id) {
        this.id = id;
    }
}
