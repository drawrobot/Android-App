package at.htl_leonding.sirdrawalot;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.util.DisplayMetrics;

import java.io.ByteArrayOutputStream;

/**
 * Created by rene on 20.07.2015.
 */
public class Utils {

    public static final int IMAGE_WIDTH = 1920;
    public static final int IMAGE_HEIGHT = 1080;

    public static int getDp(Activity mContext, int dp) {
        return (int)(dp * mContext.getResources().getDisplayMetrics().density + 0.5);
    }

    public static int halfScreenWidth(Activity mContext) {
        DisplayMetrics metrics = mContext.getResources().getDisplayMetrics();
        return (int) ((metrics.widthPixels / 2) * metrics.density + 0.5);
    }

    public static int halfScreenHeight(Activity mContext) {
        DisplayMetrics metrics = mContext.getResources().getDisplayMetrics();
        return (int) ((metrics.heightPixels / 2) * metrics.density + 0.5);
    }

    public static byte[] getBytesFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        return stream.toByteArray();
    }

    public static Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        return resizedBitmap;
    }


}
