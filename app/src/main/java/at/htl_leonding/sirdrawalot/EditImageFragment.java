package at.htl_leonding.sirdrawalot;

import android.app.Fragment;
import android.app.FragmentManager;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;

import java.nio.ByteBuffer;

/**
 * Created by rene on 24.07.2015.
 */
public class EditImageFragment extends Fragment implements View.OnClickListener {
    private static ImageView mImageView;
    private static SeekBar mSeekbar;
    private boolean mProcessing = false;
    private ConvertBitmapTask mConvertBitmapTask = new ConvertBitmapTask();
    private static Bitmap mRealImage;
    private static FragmentManager mFragmentManager;
    private Button mBtnPrint;
    private String mPos;

    public static ImageView getImageView() {
        return mImageView;
    }

    public static void setRealImage(Bitmap bitmap) {
        mRealImage = bitmap;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args.size() != 0) {
            mPos = args.getString("Position");
            new TcpClient.GetRegularSizeImageTask(this).execute(mPos);
        }
        mFragmentManager = getFragmentManager();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_edit_image, container, false);
        mImageView = (ImageView) rootView.findViewById(R.id.iv_edit_image);
        mSeekbar = (SeekBar) rootView.findViewById(R.id.seek_bar);
        mSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                startTaskEndTouch(seekBar.getProgress());
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                startTask(progress);
            }
        });
        mBtnPrint = (Button) rootView.findViewById(R.id.btn_print);
        mBtnPrint.setOnClickListener(this);
        return rootView;
    }

    public void startTask(int progress) {
        if(!mProcessing)
        {
            mProcessing = true;
            mConvertBitmapTask.cancel(true);
            mConvertBitmapTask = new ConvertBitmapTask();
            mConvertBitmapTask.execute(progress);
        }

    }

    public void startTaskEndTouch(int progress) {
        mConvertBitmapTask.cancel(true);
        mConvertBitmapTask = new ConvertBitmapTask();
        mConvertBitmapTask.execute(progress);
        mProcessing = true;
    }

    public static void onBackPressed() {
        if (mRealImage != null) {
            mRealImage = null;
            mFragmentManager.beginTransaction()
                    .replace(R.id.main_container, new GalleryFragment()).commit();
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_print) {
            sendPrint();
        }
    }

    private void sendPrint() {
        TcpClient.getInstance().startPrint(mPos,mSeekbar.getProgress());
    }

    /***
     *  Converts the Image to black and white as described in our System Specification.
     */
    public class ConvertBitmapTask extends AsyncTask<Integer, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(Integer... ints) {
            final Bitmap bmp = mRealImage.copy(mRealImage.getConfig(), true);
            final int progress = ints[0];
            final int size = bmp.getRowBytes() * bmp.getHeight() * 4;
            final byte byte255 = (byte) 255;
            ByteBuffer buf = ByteBuffer.allocate(size);
            bmp.copyPixelsToBuffer(buf);
            final byte[] bytes = buf.array();

            for (int i = 0; i < size; i += 4) {
                if (((3 * (bytes[i] & 0xFF)) + (bytes[i + 1] & 0xFF) + ((bytes[i + 2]<<1) & 0xFF)) / 6 < progress) {
                    bytes[i] = 0;
                    bytes[i + 1] = 0;
                    bytes[i + 2] = 0;
                } else {
                    bytes[i] = byte255;
                    bytes[i + 1] = byte255;
                    bytes[i + 2] = byte255;
                }
            }
            ByteBuffer retBuf = ByteBuffer.wrap(bytes);
            bmp.copyPixelsFromBuffer(retBuf);
            return bmp;
        }

        @Override
        protected void onPostExecute(Bitmap bmp) {
            mImageView.setImageBitmap(bmp);
            mProcessing = false;
        }
    }
}
