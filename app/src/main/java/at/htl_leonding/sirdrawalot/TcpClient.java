package at.htl_leonding.sirdrawalot;

import android.app.Activity;
import android.app.Fragment;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

/**
 * Created by Ferrrari on 29.11.2014.
 */
public class TcpClient {
    public static String IP = "sirdrawalot.tk";
    public static int PORT = 1521;
    private static TcpClient instance;
    private static String mReceivedText;
    private static Status mStatus;
    private static BufferedReader in;
    private static BufferedWriter out;
    private Socket socket;

    public void startPrint(final String pos, final int progress) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    TcpClient.getInstance().out.write("08" + pos + ";" + String.valueOf(progress) +"\n");
                    TcpClient.getInstance().out.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public enum Status {
        SUCCESS, FAILED, TIMEOUT
    }

    private TcpClient() {
        try {
            socket = new Socket(IP, PORT);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static TcpClient getInstance(){
        if (TcpClient.instance == null) {
            instance = new TcpClient();
        }
        return instance;
    }

    public void deleteImage(final String id) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    TcpClient.getInstance().out.write("06" + id +"\n");
                    TcpClient.getInstance().out.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public String getAllIds() {
        mReceivedText = "";
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    TcpClient.getInstance().out.write("03\n");
                    TcpClient.getInstance().out.flush();
                    mReceivedText = TcpClient.getInstance().in.readLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return mReceivedText.substring(2);
    }

    public Bitmap getSmallImageBitmap(String mId) {
        mReceivedText = "";
        try {
            TcpClient.getInstance().out.write("05" + mId + "\n");
            TcpClient.getInstance().out.flush();
            mReceivedText = TcpClient.getInstance().in.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        final byte[] decodedString = Base64.decode(mReceivedText.substring(2), Base64.DEFAULT);
        final Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;
    }

    public static class SendTextTask extends AsyncTask<String, Void, Status> {
        private Activity mActivity;
        private MaterialDialog mDialog;

        @Override
        protected TcpClient.Status doInBackground(String ... strings) {

            if (strings[0] != null && !strings[0].equals("")) {
                try {
                    TcpClient.getInstance().out.write("07" + strings[0] + "\n");
                    TcpClient.getInstance().out.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                mStatus = TcpClient.Status.SUCCESS;
            }
            else {
                mStatus = TcpClient.Status.FAILED;
            }
            return mStatus;
        }

        @Override
        protected void onPostExecute(TcpClient.Status status) {
            super.onPostExecute(status);
            TextView tvErrorText = (TextView)mDialog.getCustomView().findViewById(R.id.tv_error_text);

            if (status == TcpClient.Status.FAILED) {
                tvErrorText.setText("Sending an empty String is not allowed!");
            }
            else if (status == TcpClient.Status.SUCCESS) {
                Toast.makeText(mActivity, "Text has been sent", Toast.LENGTH_SHORT).show();
                mDialog.dismiss();
            }
        }

        public SendTextTask(Activity activity, MaterialDialog materialDialog) {
            super();
            mActivity = activity;
            mDialog = materialDialog;
        }
    }

    public static class LoginTask extends AsyncTask<String, Void, Status> {
        private Activity mActivity;
        private MaterialDialog mLoadingDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mLoadingDialog = new MaterialDialog.Builder(mActivity)
                    .autoDismiss(false)
                    .title("Logging in")
                    .progress(true, 0).build();
            mLoadingDialog.show();
        }

        @Override
        protected TcpClient.Status doInBackground(String... params) {
            mReceivedText = "";
            try {
                TcpClient.getInstance().out.write("00" + params[0] + ":" + params[1] + "\n");
                TcpClient.getInstance().out.flush();
                mReceivedText = TcpClient.getInstance().in.readLine();
            } catch (IOException e) {

            }

            if (mReceivedText.equals("")) {
                return TcpClient.Status.TIMEOUT;
            }

            if (mReceivedText.equals("00true")) {
                try {
                    TcpClient.getInstance().out.write(RegistrationIntentService.sToken + "\n");
                    TcpClient.getInstance().out.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return TcpClient.Status.SUCCESS;
            }
            return TcpClient.Status.FAILED;
        }

        @Override
        protected void onPostExecute(TcpClient.Status status) {
            super.onPostExecute(status);
            mLoadingDialog.dismiss();

            if (status == TcpClient.Status.FAILED) {
                LoginFragment.getEtUsername().getBackground().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
                LoginFragment.getEtPassword().getBackground().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
                LoginFragment.getTvErrorMsg().setText("Invalid username or password!");
            }
            else if (status == TcpClient.Status.SUCCESS) {
                mActivity.getFragmentManager().beginTransaction().setCustomAnimations(R.anim.fade_in, R.anim.fade_out).
                        replace(R.id.main_container, new GalleryFragment(), "Gallery").commit();
            }

            else if (status == TcpClient.Status.TIMEOUT) {
                LoginFragment.getTvErrorMsg().setText("Host is unreachable!");
            }
        }

        public LoginTask(Activity activity) {
            super();
            mActivity = activity;
        }
    }

    public static class SendImageTask extends AsyncTask<Intent, Void, Boolean> {
        private Fragment mFragment;
        private MaterialDialog mDialog;

        @Override
        protected void onPreExecute() {
            mDialog = new MaterialDialog.Builder(mFragment.getActivity())
                    .title("Image is uploading...")
                    .autoDismiss(false)
                    .progress(true,0)
                    .build();
            mDialog.show();
        }

        @Override
        protected Boolean doInBackground(Intent... params) {
            Uri imageUri = params[0].getData();
            Bitmap imgBmp = null;
            try {
                imgBmp = Utils.getResizedBitmap(MediaStore
                        .Images.Media.getBitmap(mFragment.getActivity().getContentResolver()
                                , imageUri),Utils.IMAGE_WIDTH,Utils.IMAGE_HEIGHT);
            } catch (IOException e) {
                e.printStackTrace();
            }
            String imgString = Base64.encodeToString(Utils.getBytesFromBitmap(imgBmp),
                    Base64.NO_WRAP);
            try {
                TcpClient.getInstance().out.write("01" + imgString + "\n");
                TcpClient.getInstance().out.flush();
                mReceivedText = TcpClient.getInstance().in.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return mReceivedText.substring(2) != null;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            mDialog.dismiss();
            if(aBoolean) {
                mFragment.getFragmentManager().beginTransaction()
                        .replace(R.id.main_container, new GalleryFragment()).commit();
            }
        }

        public SendImageTask(Fragment fragment) {
            mFragment = fragment;
        }
    }

    public static class SendCameraImageTask extends AsyncTask<Uri, Void, Boolean> {
        private Fragment mFragment;
        private Bitmap mBitmap;
        private MaterialDialog mDialog;

        @Override
        protected void onPreExecute() {
            mDialog = new MaterialDialog.Builder(mFragment.getActivity())
                    .title("Image is uploading...")
                    .autoDismiss(false)
                    .progress(true,0)
                    .build();
            mDialog.show();
        }

        @Override
        protected Boolean doInBackground(Uri... params) {
            ContentResolver cr = mFragment.getActivity().getContentResolver();
            try {
                mBitmap = Utils.getResizedBitmap(MediaStore.Images.Media.getBitmap(cr, params[0])
                        , Utils.IMAGE_WIDTH,Utils.IMAGE_HEIGHT);
            } catch (IOException e) {
                e.printStackTrace();
            }
            String imgString = Base64.encodeToString(Utils.getBytesFromBitmap(mBitmap),
                    Base64.NO_WRAP);
            try {
                TcpClient.getInstance().out.write("01" + imgString + "\n");
                TcpClient.getInstance().out.flush();
                mReceivedText = TcpClient.getInstance().in.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return mReceivedText.substring(2) != null;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            mDialog.dismiss();
            if(result) {
                mFragment.getFragmentManager().beginTransaction()
                        .replace(R.id.main_container, new GalleryFragment()).commit();
            }
        }

        public SendCameraImageTask(Fragment fragment) {
            mFragment = fragment;
        }
    }

    public static class GetRegularSizeImageTask extends AsyncTask<String, Void, Bitmap> {
        private MaterialDialog mDialog;
        private Fragment mFragment;

        @Override
        protected void onPreExecute() {
            mDialog = new MaterialDialog.Builder(mFragment.getActivity())
                    .title("Image loading...")
                    .cancelable(false)
                    .progress(true,0)
                    .build();
            mDialog.show();
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            mReceivedText = "";
            try {
                TcpClient.getInstance().out.write("02" + params[0] + "\n");
                TcpClient.getInstance().out.flush();
                mReceivedText = TcpClient.getInstance().in.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            final byte[] decodedString = Base64.decode(mReceivedText.substring(2), Base64.DEFAULT);
            final Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            return decodedByte;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            if (result != null) {
                mDialog.dismiss();
                EditImageFragment.setRealImage(result);
                EditImageFragment.getImageView().setImageBitmap(result);
            }
        }

        public GetRegularSizeImageTask(Fragment fragment) {
            mFragment = fragment;
        }
    }
}

