package at.htl_leonding.sirdrawalot;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;

import java.io.File;

/**
 * Created by rene on 24.07.2015.
 */
public class CameraFragment extends Fragment {
    private Uri mImageUri;
    private static final int RESULT_CAMERA = 100;
    private boolean mSuccess = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        File photo;
        try {
            photo = createTemporaryFile("picture", ".jpg");
            photo.delete();
            mImageUri = Uri.fromFile(photo);
        } catch (Exception e) {
            e.printStackTrace();
        }

        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
        startActivityForResult(intent, RESULT_CAMERA);
        super.onCreate(savedInstanceState);
    }

    private File createTemporaryFile(String part, String ext) throws Exception
    {
        File tempDir= Environment.getExternalStorageDirectory();
        tempDir=new File(tempDir.getAbsolutePath()+"/.temp/");
        if(!tempDir.exists())
        {
            tempDir.mkdir();
        }
        return File.createTempFile(part, ext, tempDir);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent)
    {
        if (resultCode == 0) {
            onBackPressed();
        }
        if(requestCode == RESULT_CAMERA)
        {
            mSuccess = true;
        }
        super.onActivityResult(requestCode, resultCode, intent);
    }

    private void onBackPressed() {
        getFragmentManager().beginTransaction()
                .replace(R.id.main_container, new GalleryFragment()).commit();
    }

    @Override
    public void onStart() {
        super.onStart();
        final Fragment fragment = this;
        if (mSuccess) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    TcpClient.SendCameraImageTask sendCameraImageTask = new TcpClient.SendCameraImageTask(fragment);
                    sendCameraImageTask.execute(mImageUri);
                    mSuccess = false;
                }
            }, 50);
        }
    }
}
