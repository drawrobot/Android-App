package at.htl_leonding.sirdrawalot;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.app.Activity;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.app.Fragment;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

/**
 * Fragment which retrieves all the images from the database for the user
 * and displays them in a GridView.
 */
public class GalleryFragment extends Fragment implements View.OnClickListener {

    private static FloatingActionButton mFabAdd;
    private static Toolbar mToolbar;
    private static GridView mGvGallery;
    private static Activity mContext;
    private static ImageAdapter mAdapter;
    private static String sText = "";

    public GalleryFragment() {
    }

    public static void invalidateGridViewViews() {
        mGvGallery.invalidateViews();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Image Gallery");
        ImageAdapter.cancelTasks = false;
        View rootView = inflater.inflate(R.layout.fragment_gallery, container, false);
        mFabAdd = (FloatingActionButton) rootView.findViewById(R.id.fab_add);
        mFabAdd.setOnClickListener(this);
        mContext = (Activity) rootView.getContext();

        Drawable fabIcon = getResources().getDrawable(android.R.drawable.ic_input_add);
        fabIcon.setColorFilter(getResources().getColor(R.color.icons), PorterDuff.Mode.SRC_ATOP);
        mFabAdd.setImageDrawable(fabIcon);

        mToolbar = (Toolbar) rootView.findViewById(R.id.toolbar_gallery);

        setUpImageViews(rootView);

        mGvGallery = (GridView) rootView.findViewById(R.id.gv_gallery);
        mAdapter = new ImageAdapter(getActivity(),(TcpClient.getInstance().getAllIds()).split(";"));
        mGvGallery.setAdapter(mAdapter);
        mGvGallery.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                deleteDialog(mAdapter.getItem(position).id, position);
                return true;
            }
        });
        mGvGallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ImageAdapter.cancelTasks = true;
                Bundle bundle = new Bundle();
                bundle.putString("Position", mAdapter.getItem((int) id).id);
                EditImageFragment fragment = new EditImageFragment();
                fragment.setArguments(bundle);
                getFragmentManager().beginTransaction().replace(R.id.main_container, fragment).commit();
            }
        });

        return rootView;
    }

    private void deleteDialog(final String id, final int position) {
        MaterialDialog dialog = new MaterialDialog.Builder(mContext)
                .title("Are you sure you want to delete this image?")
                .autoDismiss(true)
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        TcpClient.getInstance().deleteImage(id);
                        mAdapter.removeItem(position);
                        mAdapter.notifyDataSetChanged();

                        super.onPositive(dialog);
                    }
                }).build();
        dialog.setActionButton(DialogAction.POSITIVE, "OK");
        dialog.setActionButton(DialogAction.NEGATIVE, "CANCEL");
        dialog.show();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.fab_add) {
            animateFAB();
        }
        else if(v.getId() == R.id.iv_camera) {
            ImageAdapter.cancelTasks = true;
            getFragmentManager().beginTransaction()
                    .replace(R.id.main_container, new CameraFragment()).commit();
        }
        else if (v.getId() == R.id.iv_folder) {
            ImageAdapter.cancelTasks = true;
            getFragmentManager().beginTransaction()
                    .replace(R.id.main_container, new ImageFolderFragment()).commit();
        }
        else if (v.getId() == R.id.iv_text) {
            ImageAdapter.cancelTasks = true;
            sendText();
        }
    }

    private void sendText() {
        final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .title("Send Text")
                .customView(R.layout.dialog_send_text, true)
                .autoDismiss(false)
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(final MaterialDialog dialog) {
                        EditText etText = (EditText) dialog.getCustomView().findViewById(R.id.et_text);
                        sText = etText.getText().toString();
                        final TcpClient.SendTextTask sendTextTask = new TcpClient.SendTextTask(getActivity(), dialog);
                        sendTextTask.execute(sText);
                        etText.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                TextView tvErrorText = (TextView) dialog.getCustomView().findViewById(R.id.tv_error_text);
                                tvErrorText.setText("");
                            }
                        });
                        super.onPositive(dialog);
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        super.onNegative(dialog);
                        dialog.dismiss();
                    }
                }).build();
        dialog.setActionButton(DialogAction.POSITIVE, "SEND");
        dialog.setActionButton(DialogAction.NEGATIVE, "CANCEL");
        dialog.show();
    }

    /**
     * Initializes the ImageViews on the toolbar (accent color will be set for the ImageView icon color)
     * @param rootView
     */
    private void setUpImageViews(View rootView) {
        int accentColor = getResources().getColor(R.color.accent);

        ImageView ivCamera = (ImageView) rootView.findViewById(R.id.iv_camera);
        Drawable icCamera= getResources().getDrawable(R.drawable.ic_camera);
        icCamera.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        ivCamera.setImageDrawable(icCamera);

        ImageView ivFolder = (ImageView) rootView.findViewById(R.id.iv_folder);
        Drawable icFolder = getResources().getDrawable(R.drawable.ic_folder);
        icFolder.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        ivFolder.setImageDrawable(icFolder);

        ImageView ivText = (ImageView) rootView.findViewById(R.id.iv_text);
        Drawable icText = getResources().getDrawable(R.drawable.ic_text);
        ivText.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        ivText.setImageDrawable(icText);

        ivCamera.setOnClickListener(this);
        ivFolder.setOnClickListener(this);
        ivText.setOnClickListener(this);
    }

    /**
     * Animates the FAB in the following order:
     * -> translateAnimation up (parallel to that, the method flipFAB is called)
     * -> onAnimationEnd(...) dropFAB() is called
     * -> circularRevealFAB() is called in the end
     */
    private void animateFAB() {
        TranslateAnimation translateAnimation = new TranslateAnimation(0, 0, 0, -420);
        translateAnimation.setDuration(400);
        translateAnimation.setFillEnabled(true);
        translateAnimation.setInterpolator(new DecelerateInterpolator());
        final int newLeft = mFabAdd.getLeft();
        final int newTop = mFabAdd.getTop() - 420;
        flipFAB();
        translateAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mFabAdd.layout(newLeft, newTop, newLeft + mFabAdd.getMeasuredWidth(), newTop + mFabAdd.getMeasuredHeight());
                dropFAB();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        mFabAdd.startAnimation(translateAnimation);
    }

    /**
     * Animates "dropping" of the FAB
     */
    private void dropFAB() {
        TranslateAnimation translateAnimation = new TranslateAnimation(0, 0, 0, +420);
        translateAnimation.setDuration(400);
        translateAnimation.setFillEnabled(true);
        translateAnimation.setInterpolator(new AccelerateInterpolator());
        translateAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                final int newLeft = mFabAdd.getLeft();
                final int newTop = mFabAdd.getTop() + 420;
                mFabAdd.layout(newLeft, newTop, newLeft + mFabAdd.getMeasuredWidth(), newTop + mFabAdd.getMeasuredHeight());
                circularRevealFAB();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        mFabAdd.startAnimation(translateAnimation);
    }

    /**
     * Animates a flip (imagine it like a coinflip)
     */
    private void flipFAB() {
        int animTime = 500;
        ObjectAnimator rotateX = ObjectAnimator.ofFloat(getActivity(), "rotationX", 0.0f, 180f);
        rotateX.setDuration(animTime);
        rotateX.setInterpolator(new AccelerateDecelerateInterpolator());
        rotateX.setTarget(mFabAdd);
        rotateX.start();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mFabAdd.setBackgroundTintList(getResources().getColorStateList(R.color.fab_colorstatelist_primary));
            }
        }, animTime / 2);
    }

    /**
     * Animates the circular Reveal of the toolbar at the bottom of the fragment layout
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void circularRevealFAB() {
        int x = mToolbar.getMeasuredWidth() - mFabAdd.getWidth();
        int y = mToolbar.getMeasuredHeight() / 2;
        int radius = Math.max(mToolbar.getWidth(), mToolbar.getHeight());

        Animator anim =
                ViewAnimationUtils.createCircularReveal(mToolbar, x, y, 0, radius);
        anim.setDuration(140);
        anim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                int pad50dp = Utils.getDp(mContext, 50);
                mGvGallery.setPadding(0, pad50dp, 0, pad50dp); //otherwise the toolbar would overlap the gridview
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        mToolbar.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mFabAdd.setVisibility(View.INVISIBLE);
            }
        }, 40);

        anim.start();
    }

    public static void onBackPressed() {
        if (mFabAdd.getVisibility() == View.INVISIBLE
                && mToolbar.getVisibility() == View.VISIBLE) {
            translateAwayToolbar();
        }

    }
    private static void translateAwayToolbar() {
        TranslateAnimation translateAnimation = new TranslateAnimation(0,0,0,+300);
        translateAnimation.setDuration(150);
        translateAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mToolbar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        mToolbar.setAnimation(translateAnimation);
    }
}
